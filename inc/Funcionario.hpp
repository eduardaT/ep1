#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include <iostream>
#include <string>
#include "Pessoa.hpp"

class Funcionario: public Pessoa{
private:
    string cargo;
    int codigo;
public:
    Funcionario();
    ~Funcionario();
    
    void pesquisar();

    void set_cargo(string cargo);
    string get_cargo();

    void set_codigo(int codigo);
    int get_codigo();
};

#endif