#ifndef PRODUTO_H
#define PRODUTO_H

#include <iostream>
#include <string>

using namespace std;

class Produto{
private:
    string nome;
    string categoria;
    int quantidade;
    int codigo;
public:
    Produto();
    ~Produto();

    void cadastrar();

    void pesquisar();

    void set_nome(string nome);
    string get_nome();

    void set_categoria(string categoria);
    string get_categoria();

    void set_quantidade(int quantidade);
    int get_quantidade();

    void set_codigo(int codigo);
    int get_codigo();
};

#endif