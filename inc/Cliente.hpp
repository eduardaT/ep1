#ifndef CLIENTE_HPP
#define CLIENTE_HPP
#include <iostream>
#include <string>
#include "Pessoa.hpp"

using namespace std;

class Cliente: public Pessoa{
    private:
        string socio;

    public:
    
        Cliente();
        ~Cliente();
        
        void imprimir_dados();

        void set_socio(string socio);
        string get_socio();

        void cadastrar();

        void pesquisar();
};

#endif