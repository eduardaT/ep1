#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <iostream>
#include <string>

using namespace std;

class Pessoa{
private:
    string nome;
    string email;
    int cpf;
public:
    Pessoa();
    ~Pessoa();
    
    void cadastrar();

    void set_nome(string nome);
    string get_nome();

    void set_email(string email);
    string get_email();

    void set_cpf(int cpf);
    int get_cpf();
};

#endif