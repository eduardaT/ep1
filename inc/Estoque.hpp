#ifndef ESTOQUE_H
#define ESTOQUE_H

#include <iostream>
#include <string>
#include "Produto.hpp"

class Estoque: public Produto{
private:
    bool status;
public: 
    Estoque();
    ~Estoque();

    void verificarQuantidade();

    void set_status(bool status);
    bool get_status();
};

#endif