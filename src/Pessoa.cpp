#include "Pessoa.hpp"

Pessoa::Pessoa(){
    set_nome("");
    set_email("");
    set_cpf(0);
}

Pessoa::~Pessoa(){
    cout << "Destrutor" << endl;
}

void Pessoa::set_nome(string nome){
    this -> nome = nome;
}
string Pessoa::get_nome(){
    return this -> nome;
}

void Pessoa::set_email(string email){
    this -> email = email;
}
string Pessoa::get_email(){
    return (this -> email);
}

void Pessoa::set_cpf(int cpf){
    this -> cpf = cpf;
}
int Pessoa::get_cpf(){
    return (this -> cpf);
}

